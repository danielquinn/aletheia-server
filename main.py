import base64
import logging
import os

from tempfile import NamedTemporaryFile
from typing import AnyStr, Tuple

from fastapi import FastAPI, File, HTTPException, UploadFile
from starlette.responses import FileResponse

from aletheia import utils as aletheia
from aletheia.exceptions import (
    NotJsonObjectError,
    PrivateKeyNotDefinedError,
    PublicKeyNotExistsError,
    UnacceptableLocationError,
    UnknownFileTypeError,
    UnparseableFileError,
    UnrecognisedKeyError,
)


logger = logging.getLogger(__name__)

VERSION = (0, 0, 1)

# A list of all possible EXCEPTIONS that could break while signing/verifying
EXCEPTIONS = (
    NotJsonObjectError,
    PrivateKeyNotDefinedError,
    PublicKeyNotExistsError,
    UnacceptableLocationError,
    UnknownFileTypeError,
    UnparseableFileError,
    UnrecognisedKeyError,
)

# FastAPI requires this
app = FastAPI()


@app.on_event("startup")
async def check_environment():
    domain, private_key = await get_config_from_environment()
    if not domain or not private_key:
        logger.warning(
            "This server is starting without an ALETHEIA_DOMAIN or "
            "ALETHEIA_PRIVATE_KEY environment variable set.  It can therefore "
            "only be used to verify files."
        )


@app.get("/")
async def index():
    version = ".".join([str(_) for _ in VERSION])
    return {"hello": f"Aletheia Server {version}"}


@app.post("/verify")
async def verify(file: UploadFile = File(...)):

    tmp = await localise_file(file)

    try:
        origin = aletheia.verify(tmp.name)
    except EXCEPTIONS as e:
        os.unlink(tmp.name)
        raise HTTPException(status_code=400, detail=str(e))

    os.unlink(tmp.name)

    return {"origin": origin}


@app.post("/sign")
async def sign(file: UploadFile = File(...)):

    domain, private_key = await get_config_from_environment()

    if not domain or not private_key:
        return HTTPException(
            status_code=501,
            detail="This server hasn't been configured for signing."
        )

    tmp = await localise_file(file)
    try:
        aletheia.sign(tmp.name, private_key)
    except EXCEPTIONS as e:
        os.unlink(tmp.name)
        raise HTTPException(status_code=400, detail=str(e))

    return FileResponse(tmp.name)


async def localise_file(file: UploadFile = File(...)) -> NamedTemporaryFile:
    """
    Aletheia needs to work on a local file, not a stream, so here we take the
    uploaded file and write it to a local named temporary file.
    """

    tmp = NamedTemporaryFile(mode="wb", delete=False)
    tmp.write(file.file.read())
    tmp.close()

    return tmp


async def get_config_from_environment() -> Tuple[AnyStr, AnyStr]:

    domain = os.getenv("ALETHEIA_SIGN_DOMAIN")
    private_key = base64.decodebytes(
        bytes(os.getenv("ALETHEIA_PRIVATE_KEY", ""), "utf-8"))

    return domain, private_key
