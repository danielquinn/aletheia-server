from unittest import TestCase

from starlette.testclient import TestClient

from .main import app

client = TestClient(app)


class AletheiaServerTestCase(TestCase):

    def test_read_main(self):
        """
        Mocking main.VERSION didn't appear to play well with how FastAPI does
        things, so I had to use a regex to test the index output :-(
        """

        response = client.get("/")

        self.assertEqual(response.status_code, 200)

        payload = response.json()
        keys = tuple(payload.keys())
        values = tuple(payload.values())

        self.assertEqual(keys, ("hello",))
        self.assertRegex(values[0], r"^Aletheia Server \d+.\d+.\d+$")

    def test_verify_no_key(self):
        pass

    def test_verify_bad_key(self):
        pass

    def test_verify_good_key(self):
        pass
