FROM python:3.8-alpine

RUN \
  apk add --update --no-cache --virtual .build-dependencies \
    libffi-dev \
    openssl-dev \
    build-base \
  && apk add --no-cache \
    ffmpeg \
    exiftool \
    ffmpeg \
    exiftool \
    libmagic \
  && pip install --no-cache-dir \
    fastapi \
    uvicorn \
    python-multipart \
    aiofiles \
    aletheia \
  && pip uninstall --yes file-magic \
  && pip install --no-cache-dir python-magic \
  && apk del .build-dependencies \
  && addgroup -g 1000 aletheia \
  && adduser -D -u 1000 -G aletheia -h /app aletheia

# Check that Aletheia is working
RUN wget https://danielquinn.org/static/danielquinn/img/me.jpg \
  && aletheia verify me.jpg \
  && rm me.jpg

COPY . /app

WORKDIR /app
CMD ["uvicorn", "main:app", "--host", "0.0.0.0"]

EXPOSE 8000

